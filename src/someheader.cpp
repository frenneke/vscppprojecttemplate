/**
 * @file someheader.cpp
 * @author Florian Renneke (florianrenneke@gmail.com)
 * @brief Header file
 * @version 0.1
 * @date 2021-03-08
 * 
 * @copyright Copyright (c) 2021 Florian Renneke
 * 
 */

#include "someheader.hpp"

CHeader::CHeader(int k) : val(k)
{
}

CHeader::~CHeader()
{
}

int CHeader::add(int l)
{
    return val+l+2+1;
}
