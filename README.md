# Project Template

## Compiling from Source
To compile from source and running the tests enter the following commands:
```
mkdir build
cd build
cmake ..
make
make test
```

