/**
 * @file someheader.hpp
 * @author Florian Renneke (florianrenneke@gmail.com)
 * @brief Header file
 * @version 0.1
 * @date 2021-03-08
 * 
 * @copyright Copyright (c) 2021 Florian Renneke
 */

#pragma once

#include "common.hpp"


class CHeader
{
private:
    int val;
public:
    CHeader(int k);
    ~CHeader();

    int add(int l);
};
