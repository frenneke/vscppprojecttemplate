/**
 * @file common.hpp
 * @author Florian Renneke (florianrenneke@gmail.com)
 * @brief Precompiled header file for all non-changing header files (stdlib and other libs)
 * @version 0.1
 * @date 2021-03-08
 * 
 * @copyright Copyright (c) 2021 Florian Renneke
 */

#include <string>
#include <sstream>
#include <fstream>
#include <iostream>
#include <regex>
#include <filesystem>
#include <memory>
#include <vector>
#include <map>

#include <CLI/App.hpp>
#include <CLI/Formatter.hpp>
#include <CLI/Config.hpp>

#include "spdlog/spdlog.h"
#include "spdlog/sinks/basic_file_sink.h"
#include "spdlog/sinks/stdout_color_sinks.h"

//#include <boost/asio.hpp>
//#include <boost/asio/ssl.hpp>

#include <wx/wxprec.h>
#ifndef WX_PRECOMP
    #include <wx/wx.h>
#endif