/**
 * @file test0.cpp
 * @author Florian Renneke (florianrenneke@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2021-03-09
 * 
 * @copyright Copyright (c) 2021 Florian Renneke
 */


#include <gtest/gtest.h>
#include "someheader.hpp"

using namespace std;

TEST(blaTest, test0) {
    CHeader h(10);

    cout << "hi " << endl;
    EXPECT_EQ (h.add(0),  10);
    EXPECT_EQ (h.add(10), 20);
    EXPECT_EQ (h.add(50), 60);
}

TEST(blaTest, test00) {
    CHeader h(10);

    cout << "hi " << endl;
    EXPECT_EQ (h.add(0),  10);
    EXPECT_EQ (h.add(10), 20);
    EXPECT_EQ (h.add(50), 60);
}


TEST(different, test0) {
    CHeader h(10);

    cout << "hi " << endl;
    EXPECT_EQ (h.add(0),  10);
    EXPECT_EQ (h.add(10), 20);
    EXPECT_EQ (h.add(50), 60);
}
TEST(different, test1) {
    CHeader h(10);

    cout << "hi " << endl;
    EXPECT_EQ (h.add(0),  10);
    EXPECT_EQ (h.add(10), 20);
    EXPECT_EQ (h.add(50), 60);
}